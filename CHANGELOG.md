# Kubeadm cookbook changelog file

## Unreleased

 - Install given Kubernetes version
 - Supports flannel CNI driver
 - Supports Kubernetes upgrade
 - Supports mutli master
