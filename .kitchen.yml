---
driver:
  name: vagrant

provisioner:
  # You may wish to disable always updating cookbooks in CI or other testing environments.
  # For example:
  #   always_update_cookbooks: <%= !ENV['CI'] %>
  always_update_cookbooks: true
  chef_license: accept
  log_level: info
  name: chef_zero
  product_name: chef
  product_version: 17.9.26

verifier:
  name: inspec

platforms:
  - name: debian/stretch64
  - name: debian/buster64
  - name: ubuntu/bionic64

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Default master and worker attributes
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.master: &master
  driver:
    customize:
      memory: 2048
    network:
      - ["private_network", { ip: "172.28.128.200" }]
    vm_hostname: master-flannel.vagrant.test
  run_list:
    - recipe[apt]
    - recipe[docker-ce]
    - recipe[kubeadm::master]
  verifier:
    inspec_tests:
      - test/integration/master/flannel_test.rb
.multi-master1: &multi-master1
  driver:
    customize:
      memory: 2048
    network:
      - ["private_network", { ip: "172.28.128.200" }]
    vm_hostname: master-1-flannel.vagrant.test
  provisioner:
    nodes_path: test/fixtures/multi-master/phase-1/flannel
  run_list:
    - recipe[apt]
    - recipe[docker-ce]
    - recipe[kubeadm::master]
  verifier:
    inspec_tests:
      - test/integration/multi-master/centralized/master-1/flannel_test.rb
.multi-master2: &multi-master2
  driver:
    customize:
      memory: 2048
    network:
      - ["private_network", { ip: "172.28.128.201" }]
    vm_hostname: master-2-flannel.vagrant.test
  provisioner:
    nodes_path: test/fixtures/multi-master/phase-2/flannel
  run_list:
    - recipe[apt]
    - recipe[docker-ce]
    - recipe[kubeadm::master]
  verifier:
    inspec_tests:
      - test/integration/multi-master/centralized/master-2/flannel_test.rb

.worker: &worker
  driver:
    customize:
      memory: 2048
    network:
      - ["private_network", { ip: "172.28.128.220" }]
    vm_hostname: worker-flannel.vagrant.test
  provisioner:
    nodes_path: test/fixtures/nodes/flannel
  run_list:
    - recipe[apt]
    - recipe[docker-ce]
    - recipe[kubeadm::worker]
  verifier:
    inspec_tests:
      - test/integration/worker_test.rb
.multi-master-worker: &multi-master-worker
  driver:
    customize:
      memory: 2048
    network:
      - ["private_network", { ip: "172.28.128.220" }]
    vm_hostname: worker-flannel.vagrant.test
  provisioner:
    nodes_path: test/fixtures/multi-master/phase-2/flannel
  run_list:
    - recipe[apt]
    - recipe[docker-ce]
    - recipe[kubeadm::worker]
  verifier:
    inspec_tests:
      - test/integration/worker_test.rb
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

suites:
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Kubernetes install
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  #
  # Latest Kubernetes
  - name: k8s-latest-master-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: master # Flannel YAML 0.11.0 isn't compatible with k8s 1.16
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        psp: true
        version: latest
    verifier:
      inspec_tests:
        - test/integration/master/flannel_psp_test.rb
  - name: k8s-latest-mutli-master-1-flannel
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: master # Flannel YAML 0.11.0 isn't compatible with k8s 1.16
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.200
          unicast_peer:
            - 172.28.128.201
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        psp: true
        version: latest
    verifier:
      inspec_tests:
        - test/integration/multi-master/centralized/master-1/flannel_psp_test.rb
  - name: k8s-latest-mutli-master-2-flannel
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: master # Flannel YAML 0.11.0 isn't compatible with k8s 1.16
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.201
          unicast_peer:
            - 172.28.128.200
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        psp: true
        version: latest
    verifier:
      inspec_tests:
        - test/integration/multi-master/centralized/master-2/flannel_psp_test.rb
  - name: k8s-latest-mutli-master-worker-flannel
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: latest
  - name: k8s-latest-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: latest

  #
  # Kubernetes 1.22.x
  - name: k8s-1-22-master-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.22.0
  - name: k8s-1-22-mutli-master-1-flannel
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        keepalived:
          unicast_src_ip: 172.28.128.200
          unicast_peer:
            - 172.28.128.201
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        version: 1.22.0
  - name: k8s-1-22-mutli-master-2-flannel
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        keepalived:
          unicast_src_ip: 172.28.128.201
          unicast_peer:
            - 172.28.128.200
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        version: 1.22.0
  - name: k8s-1-22-mutli-master-worker-flannel
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        master_ip_address: 172.28.128.10
        multi_master: centralized
        version: 1.22.0
  - name: k8s-1-22-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.22.0
  #
  # Kubernetes 1.21.x
  - name: k8s-1-21-master-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.21.0
  - name: k8s-1-21-mutli-master-1-flannel
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        keepalived:
          unicast_src_ip: 172.28.128.200
          unicast_peer:
            - 172.28.128.201
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        version: 1.21.0
  - name: k8s-1-21-mutli-master-2-flannel
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        keepalived:
          unicast_src_ip: 172.28.128.201
          unicast_peer:
            - 172.28.128.200
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        version: 1.21.0
  - name: k8s-1-21-mutli-master-worker-flannel
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        master_ip_address: 172.28.128.10
        multi_master: centralized
        version: 1.21.0
  - name: k8s-1-21-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.21.0
  #
  # Kubernetes 1.20.x
  - name: k8s-1-20-master-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.20.0
  - name: k8s-1-20-mutli-master-1-flannel
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        keepalived:
          unicast_src_ip: 172.28.128.200
          unicast_peer:
            - 172.28.128.201
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        version: 1.20.0
  - name: k8s-1-20-mutli-master-2-flannel
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        keepalived:
          unicast_src_ip: 172.28.128.201
          unicast_peer:
            - 172.28.128.200
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        version: 1.20.0
  - name: k8s-1-20-mutli-master-worker-flannel
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        master_ip_address: 172.28.128.10
        multi_master: centralized
        version: 1.20.0
  - name: k8s-1-20-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.20.0
  #
  # Kubernetes 1.19.x
  - name: k8s-1-19-master-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.19.0
  - name: k8s-1-19-mutli-master-1-flannel
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.200
          unicast_peer:
            - 172.28.128.201
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.19.0
  - name: k8s-1-19-mutli-master-2-flannel
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.201
          unicast_peer:
            - 172.28.128.200
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.19.0
  - name: k8s-1-19-mutli-master-worker-flannel
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.19.0
  - name: k8s-1-19-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.19.0
  #
  # Kubernetes 1.18.x
  - name: k8s-1-18-master-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.18.0
  - name: k8s-1-18-mutli-master-1-flannel
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.200
          unicast_peer:
            - 172.28.128.201
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.18.0
  - name: k8s-1-18-mutli-master-2-flannel
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.201
          unicast_peer:
            - 172.28.128.200
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.18.0
  - name: k8s-1-18-mutli-master-worker-flannel
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.18.0
  - name: k8s-1-18-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.18.0
  #
  # Kubernetes 1.17.x
  - name: k8s-1-17-master-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.17.4
  - name: k8s-1-17-mutli-master-1-flannel
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.200
          unicast_peer:
            - 172.28.128.201
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.17.4
  - name: k8s-1-17-mutli-master-2-flannel
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.201
          unicast_peer:
            - 172.28.128.200
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.17.4
  - name: k8s-1-17-mutli-master-worker-flannel
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.17.4
  - name: k8s-1-17-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.17.4
  #
  # Kubernetes 1.16.x
  - name: k8s-1-16-master-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.16.15
  - name: k8s-1-16-mutli-master-1-flannel
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.200
          unicast_peer:
            - 172.28.128.201
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.16.15
  - name: k8s-1-16-mutli-master-2-flannel
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.201
          unicast_peer:
            - 172.28.128.200
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.16.15
  - name: k8s-1-16-mutli-master-worker-flannel
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.16.15
  - name: k8s-1-16-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.16.15
  #
  # Kubernetes 1.15.x
  - name: k8s-1-15-master-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.15.11
  - name: k8s-1-15-mutli-master-1-flannel
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.200
          unicast_peer:
            - 172.28.128.201
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.15.11
  - name: k8s-1-15-mutli-master-2-flannel
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        keepalived:
          unicast_src_ip: 172.28.128.201
          unicast_peer:
            - 172.28.128.200
          slack_webhook_url: https://hooks.slack.com/services/A043FVV6A/BBQ1S0FF/KubeAdmToken
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.15.11
  - name: k8s-1-15-mutli-master-worker-flannel
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        master_ip_address: 172.28.128.10
        multi_master: centralized
        pod_cidr: 10.244.0.0/16
        version: 1.15.11
  - name: k8s-1-15-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.15.11
  #
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Kubernetes install
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Kubernetes upgrade
  #
  # This part describes the attributes of the converge attributes which will
  # engage the Kubernetes upgrade.
  #
  # The `bin/test-kubernetes-upgrade` script first uses the
  # `.kitchen.upgrades.yml` file in order to create a master/worker cluster and
  # then uses this `.kitchen.yml` file in order to converge the nodes with the
  # updated attributes triggering the Kubernetes upgrade.
  # See the `.kitchen.upgrades.yml` file and the `bin/test-kubernetes-upgrade`
  # script.
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  #
  # Upgrades Kubernetes from 1.19 to 1.20
  - name: k8s-upgrade-1-20-mutli-master-1-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.20.1
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-20_flannel/master_test.rb
  - name: k8s-upgrade-1-20-mutli-master-2-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.20.1
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-20_flannel/master_test.rb
  - name: k8s-upgrade-1-20-mutli-master-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.20.1
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-20_flannel/worker_test.rb
  #
  # Upgrades Kubernetes from 1.18 to 1.19
  - name: k8s-upgrade-1-19-mutli-master-1-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.19.6
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-19_flannel/master_test.rb
  - name: k8s-upgrade-1-19-mutli-master-2-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.19.6
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-19_flannel/master_test.rb
  - name: k8s-upgrade-1-19-mutli-master-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.19.6
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-19_flannel/worker_test.rb
  #
  # Upgrades Kubernetes from 1.17 to 1.18
  - name: k8s-upgrade-1-18-mutli-master-1-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.18.14
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-18_flannel/master_test.rb
  - name: k8s-upgrade-1-18-mutli-master-2-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.18.14
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-18_flannel/master_test.rb
  - name: k8s-upgrade-1-18-mutli-master-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.18.14
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-18_flannel/worker_test.rb
  #
  # Upgrades Kubernetes from 1.16 to 1.17
  - name: k8s-upgrade-1-17-mutli-master-1-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.17.16
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-17_flannel/master_test.rb
  - name: k8s-upgrade-1-17-mutli-master-2-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.17.16
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-17_flannel/master_test.rb
  - name: k8s-upgrade-1-17-mutli-master-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.17.16
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-17_flannel/worker_test.rb
  #
  # Upgrades Kubernetes from 1.15 to 1.16
  - name: k8s-upgrade-1-16-mutli-master-1-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.16.15
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-16_flannel/master_test.rb
  - name: k8s-upgrade-1-16-mutli-master-2-flannel
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: flannel
          version: 0.12.0
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.16.15
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-16_flannel/master_test.rb
  - name: k8s-upgrade-1-16-mutli-master-worker-flannel
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        pod_cidr: 10.244.0.0/16
        version: 1.16.15
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-16_flannel/worker_test.rb
  #
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Kubernetes upgrade
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
