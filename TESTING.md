# Testing

This cookbook needs real systems therefore only the kitchen vagrant driver is an option.

This cookbook has 3 testing modes:

* Kubernetes single-master/worker test
* Kubernetes multi-master/worker test
* Kubernetes multi-master/worker upgrade test

Also this cookbook supports different Kubernetes CNIs, therefor many kitchen files have been created for each of them.

## Prepare the environment

1. Install Ruby if it's not yet done (See `.ruby-version`)
2. Install Bundler
    ```
    $ gem install bundler --no-document
    ```
3. Install Kitchen CI and dependencies:
    ```
    bundle
    ```
4. Install Vagrant from https://www.vagrantup.com/downloads.html
5. Install VirtualBox from https://www.virtualbox.org/wiki/Downloads

## Running the tests

### Kubernetes single-master/worker test

Here is the way to run this testing mode:

```
$ ./bin/test-kubernetes-install
```

This will test the master convergence, then keep it running, and testing the worker convergence.

In order to run it for different Kubernetes versions, see the comments in the `./bin/test-kubernetes-install` file.

Script steps:

1. Bootstrap a master node in the given version
2. Verify the master node
3. Updates the `test/fixtures/nodes/master-flannel.vagrant.test.json` file with the Discovery CA cert hash
4. Bootstrap a worker node in the same version as the master node, and join the master
5. Verify the worker node

### Kubernetes multi-master/worker test

Here is the way to run this testing mode:

```
$ ./bin/test-kubernetes-mutli-master
```

This will converge a master as the first control plane with a keepalived/haproxy in order to provide a Load Balancer, then it will converge a Kubernetes worker that will join the existing master (through the LB), and finally it will converge a second master joining the first one.

In order to run it for different Kubernetes versions, see the comments in the `./bin/test-kubernetes-mutli-master` file.

Script steps:

1. Bootstrap a master node in the given version with the `--upload-certs` flag
2. Verify the master node
3. Updates the `test/fixtures/nodes/master-flannel.vagrant.test.json` file with the token and the Discovery CA cert hash
4. Bootstrap a worker node in the same version as the master node, and join the master
5. Verify the worker node
6. Bootstrap a second master node in the given version with the `--control-plane` flag, and join the first master
7. Verify the second master node

### Kubernetes multi-master/worker upgrade test

The only way to get it working I could figure out is:
1. Create a master(s)/worker cluster using given version number minus 1 (if you'd like to test Kubernetes 1.20, the cluster is created with Kubernetes 1.19), which is done using the `.kitchen.upgrades.yml` file.
2. Upgrade the cluster using the `.kitchen.yml` file (which tell to upgrade to the requested version)

Here is the way to run this testing mode:

```
$ UPGRADE_KUBERNETES_TO_VERSION='1.16' ./bin/test-kubernetes-upgrade
```

_*Note:* See the bin file for available options to be passed to the script._

This command will test upgrading a Kubernetes 1.15 cluster to 1.16.

Script steps:

1. Bootstrap a 1.15 master
2. Bootstrap a 1.15 worker and join the master
3. Bootstrap a 1.15 second master node and join the first master
4. Updates the `test/fixtures/nodes/master-flannel.vagrant.test.json` file with all required credentials
5. Upgrades the Kubernetes master
6. Verify the master
7. Upgrades the second Kubernetes master
8. Verify the second master
9. Upgrades the Kubernetes worker
10. Verify the worker
