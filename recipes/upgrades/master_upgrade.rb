# frozen_string_literal: true

# `build_kubeadm_version`
Chef::Recipe.send(:include, KubeadmCookbook::Helper)
# `kubernetes_version_from_attributes`
# `no_upgraded_master_yet?`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Helper)
# `stream_command`
# `stream_command_and_watch_for`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::StreamCommand)

ruby_block 'warning about migration' do
  action :run
  block do
    Chef::Log.warn 'kubeadm: Running the Kubernetes master upgrade ...'
  end
end

ruby_block 'kubectl get nodes' do
  action :run
  block do
    stream_command('kubectl get nodes') do |output|
      Chef::Log.info "kubeadm: #{output.gsub(/\r\n/, '').gsub(/\t/, '  ')}"
    end
  end
end

ruby_block 'kubeadm drain node' do
  action :run
  block do
    command_flags = '--ignore-daemonsets'

    command_flags += if current_kubernetes_version.tr('_', '.').to_f > 1.20
                       ' --delete-emptydir-data'
                     else
                       ' --delete-local-data'
                     end

    node.run_state['node_drained'] = stream_command_and_watch_for(
      "kubectl drain `hostname` #{command_flags}",
      /drained/
    )
  end
end

#
# First control plane upgrade
#
ruby_block 'kubeadm upgrade plan' do
  action :run
  block do
    node.run_state['ready_to_migrate'] = stream_command_and_watch_for(
      "kubeadm upgrade plan v#{kubernetes_version_from_attributes}",
      /You can now apply the upgrade/
    )
  end
  only_if { node.run_state['node_drained'] }
  only_if { no_upgraded_master_yet? }
end

ruby_block 'kubeadm upgrade apply' do
  action :run
  block do
    node.run_state['upgrade_done'] = stream_command_and_watch_for(
      "kubeadm upgrade apply v#{kubernetes_version_from_attributes} --yes",
      /SUCCESS! Your cluster was upgraded/
    )
  end
  only_if { no_upgraded_master_yet? }
  only_if { node.run_state['ready_to_migrate'] == true }
end

#
# Other control planes upgrade
#
ruby_block 'kubeadm upgrade node' do
  action :run
  block do
    node.run_state['upgrade_done'] = stream_command_and_watch_for(
      'kubeadm upgrade node',
      /SUCCESS! Your cluster was upgraded/
    )
  end
  only_if { node.run_state['node_drained'] }
  not_if { no_upgraded_master_yet? }
end

ruby_block 'kubectl uncordon node' do
  action :run
  block do
    node.run_state['node_uncordoned'] = stream_command_and_watch_for(
      'kubectl uncordon `hostname`',
      /uncordoned/
    )
  end
  only_if { node.run_state['upgrade_done'] == true }
end

package 'kubectl' do
  action %i[unlock install lock]
  version(lazy { build_kubeadm_version })
  only_if { node.run_state['node_uncordoned'] == true }
end

package 'kubelet' do
  action %i[unlock install lock]
  version(lazy { build_kubeadm_version })
  notifies :restart, 'service[restart kubelet]', :immediately
  only_if { node.run_state['node_uncordoned'] == true }
end

service 'restart kubelet' do
  service_name 'kubelet'
  supports status: true
  action [:nothing]
end
