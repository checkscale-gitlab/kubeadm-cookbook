# frozen_string_literal: true

include_recipe 'kubeadm::common'

# `current_kubernetes_version`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Helper)

ruby_block 'populate run_state role' do
  action :run
  block do
    node.run_state['role'] = 'master'
  end
end

case node['kubeadm']['multi_master']
when 'off'
  # Single master node, no need for keepalived nor HAproxy.
  #
  # Determines kubeadm action (init or join).
  include_recipe 'kubeadm::install_determine_action'
when 'centralized'
  # Only installs keepalived and HAproxy on master nodes. Workers knows the
  # Virtual IP address (from keepalived).
  include_recipe 'kubeadm::install_centralized_multi_master_environment'
when 'decentralized'
  # Installs an HAproxy instance, configured with master nodes, on all the
  # nodes.
  #
  # TODO : Install HAproxy on master nodes too (today it is installed on worker
  #        nodes only, making master nodes pointing to the master node that has
  #        initialiazed the cluster, meaning SPOF!)
  include_recipe 'kubeadm::install_determine_action'
when 'balanced'
  # Do not install any thing, all nodes refers to the external load balancer.
  include_recipe 'kubeadm::install_determine_action'
else
  Chef::Log.fatal 'kubeadm: Unknown multi_master mode ' \
                  "'#{node['kubeadm']['multi_master']}'. Please update your " \
                  'configuration and set the kubeadm multi_master attribute ' \
                  'to "off", "centralized" or "decentralized". (See the ' \
                  'kubeadm cookbook documentation).'

  raise
end

ruby_block 'master action' do
  action :run
  block do
    master_action = 'install_master'

    if node.run_state['upgrading_kubernetes']
      #
      # This cookbook support defining upgrade recipe for a specific Kubernetes
      # version.
      #
      # For example, if you would need it for Kubernetes 1.20 you would have to
      # create the file `upgrades/master_upgrade_1-20.rb` and it would be used
      # instead of the general `upgrades/master_upgrade.rb` file.
      master_action = "master_upgrade_#{current_kubernetes_version}"

      unless File.exist?(File.join(__dir__, 'upgrades', master_action))
        master_action = 'master_upgrade'
      end
    end

    run_context.include_recipe "kubeadm::#{master_action}"
  end
end
