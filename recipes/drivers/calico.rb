# frozen_string_literal: true

# `calico_url_with_version`
Chef::Resource::RemoteFile.send(:include, KubeadmCookbook::Calico)
# `kubernetes_master_reachable?`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Helper)
# `wait_command_output`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::StreamCommand)
# `update_flannel_config_file_at`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Flannel)

ruby_block 'checking if Kubernetes is up' do
  action :run
  block do
    kube_up = kubernetes_master_reachable?(local: true)

    Chef::Log.warn "kubeadm: Kubernetes is#{' not' unless kube_up} running."

    node.run_state['kube_up'] = kube_up
  end
  not_if { node['kubeadm']['multi_master'] == 'off' }
  only_if { node.run_state['first_control_plane'] }
end

package 'wireguard' do
  action :install
  only_if { node['kubeadm']['cni_driver']['calico']['wireguard']['enable'] }
end

#
# Installs the Calico network driver to an existing Kubernetes cluster.
#
# Download manifest to /tmp/kube-calico.yml
remote_file '/tmp/kube-calico.yml' do
  action :create
  source(lazy { calico_url_with_version })
  owner 'root'
  group 'root'
  mode '0644'

  only_if { node.run_state['first_control_plane'] }

  only_if do
    if node['kubeadm']['multi_master'] == 'off'
      true
    else
      Chef::Log.warn 'kubeadm: [remote_file] In multi-master mode.'

      Chef::Log.warn "kubeadm: [remote_file] node.run_state['kube_up']: #{node.run_state['kube_up'].inspect}"
      if node.run_state['kube_up']
        # Ensures the master node is in the Ready state
        Chef::Log.warn 'kubeadm: Checking master node status in Kubernetes ...'
        ready_shell = shell_out('kubectl get node $(hostname) | tail -n 1 | ' \
                                "awk '{print $2}' | grep Ready")
        if ready_shell.exitstatus.zero?
          Chef::Log.warn 'kubeadm: Master node is in Ready state'

          # Checks if the Calico pods are present and in the Running state
          Chef::Log.warn 'kubeadm: Checking Calico pods status ...'
          status_shell = shell_out('kubectl get pods -n kube-system ' \
                                   '| grep calico-kube-controllers | grep Running')
          if status_shell.exitstatus.zero?
            Chef::Log.warn 'kubeadm: Calico pod is present and in the ' \
                           'Running state, abording Calico installation ...'
            false
          else
            Chef::Log.warn 'kubeadm: Calico pods are not present, or not in ' \
                           'the Running state! (Exit code: ' \
                           "#{status_shell.exitstatus})"
            Chef::Log.warn "kubeadm: #{status_shell.stdout.inspect}"

            Chef::Log.warn 'Running the Calico installation!'
            true
          end
        else
          Chef::Log.warn 'kubeadm: Master node is not in Ready state, ' \
                         'abording Calico installation ...'

          false
        end
      else
        false
      end
    end
  end
end

execute 'kubectl apply -f /tmp/kube-calico.yml' do
  command 'kubectl apply -f /tmp/kube-calico.yml'
  action :run
  only_if 'test -f /tmp/kube-calico.yml'
end

# Wait for the Calico pod to be running. Name is something like
# `calico-kube-controllers-69496d8b75-d522g`.
ruby_block 'wait for calico' do
  action :run
  block do
    Chef::Log.warn 'kubeadm: Waiting for the Calico containers to boot ' \
                   '(timeout after 120 seconds) ...'

    command = <<~CMD
      kubectl get pods --namespace=kube-system | \
      grep calico-kube-controllers | \
      awk '{print $3}'
    CMD

    begin
      # Runs the given command, and look at its ouput if it matches the expected
      # regex until the end of the timeout (120 seconds).
      # Each time the output changes, the block is executed with its output.
      wait_command_output(command, /Running/, timeout: 120) do |output|
        Chef::Log.warn "kubeadm: Calico container status changed to #{output}."
      end
    rescue KubeadmCookbook::WaitCommandTimeoutError
      # When the timeout is reached, the WaitCommandTimeoutError is thrown
      Chef::Log.warn 'kubeadm: Waiting for the Calico Kubernetes container ' \
                     'to be "Running" failed after 120 seconds.'
      Chef::Log.warn "kubeadm: It doesn't mean that the deployment failed, " \
                     'but you should check manually on the node the Calico ' \
                     'container status.'
      Chef::Log.warn 'kubeadm: You can have a look at the existing ' \
                     'containers with the following command:'
      Chef::Log.warn 'kubeadm: kubectl get pods --namespace=kube-system'
    end
  end
  only_if 'test -f /tmp/kube-calico.yml'
end

file '/tmp/kube-calico.yml' do
  action :delete
  only_if 'test -f /tmp/kube-calico.yml'
end

ruby_block 'configure Calico with wireguard' do
  action :run
  block do
    patch = shell_out(
      <<~CMD
        kubectl patch --type=merge FelixConfiguration default \
                      --namespace global \
                      --patch '{"spec":{"wireguardEnabled":true}}'
      CMD
    )

    if patch.exitstatus.positive?
      Chef::Log.fatal 'kubeadm: Calico patching command exited with ' \
                      "#{patch.exitstatus}: #{patch.stderr}"
      exit 1
    else
      Chef::Log.info 'kubeadm: Wireguard successfully enabled in Calico'
    end
  end
  only_if { node['kubeadm']['cni_driver']['calico']['wireguard']['enable'] }
  only_if { node.run_state['first_control_plane'] }
end
