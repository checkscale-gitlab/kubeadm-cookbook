# frozen_string_literal: true

# `build_kubeadm_version` in `package`
Chef::Recipe.send(:include, KubeadmCookbook::Helper)
# `node_has_systemd?`
# `systemd_has_swap_unit?`
# `systemd_swap_unit_is_running?`
# `systemd_swap_unit_name`
Chef::Recipe.send(:include, KubeadmCookbook::Systemd)
# `upgrading_kubernetes?` in `package`
Chef::Resource::AptPackage.send(:include, KubeadmCookbook::Helper)
# `upgrading_kubernetes?` in `ruby_block`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Helper)

# Checks that `external_lb_address` and `external_lb_port` attributes are
# defined when `multi_master` is `balanced` otherwise raise an error
ruby_block 'ensure external load balancer is configured' do
  action :run
  block do
    next unless node['kubeadm']['external_lb_address'].nil? ||
                node['kubeadm']['external_lb_address'].empty?

    Chef::Log.fatal "kubeadm: You have configured the 'multi_master' in " \
                    "'balanced' mode, but the 'external_lb_address' is empty " \
                    "which is invalid. Please give the address or IP address " \
                    "of your load balancer, or change the 'multi_master' " \
                    "mode and converge the node again."

    raise
  end
  only_if { node['kubeadm']['multi_master'] == 'balanced' }
end

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# System preparation
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# => Disables swap
if node_has_systemd?
  if systemd_has_swap_unit? && systemd_swap_unit_is_running?
    systemd_unit systemd_swap_unit_name do
      action %i[stop mask]
    end
  end
else
  # This is not a permanent way to disable the swap
  execute 'disable swap' do
    action :run
    command 'swapoff -a'
    not_if 'swapon -s | wc -l | grep 0'
  end
end
#
# => Disables firewalld
service 'firewalld' do
  action %i[disable stop]
  supports status: true
end
#
# => Disables selinux
execute 'selinux' do
  action :run
  command 'setenforce 0'
  not_if 'getenforce | grep Permissive'
  only_if { ::File.exist?('/etc/selinux/config') }
end
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# kubeadm installation
# See https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# => Installs dependencies to prepare the system to install kubeadm
package 'apt-transport-https' do
  action :install
end
#
# => Adds the Kubernetes APT repository
apt_repository 'kubernetes' do
  action        :add
  arch          'amd64'
  components    ['main']
  distribution  'kubernetes-xenial'
  key           'https://packages.cloud.google.com/apt/doc/apt-key.gpg'
  uri           'https://apt.kubernetes.io/'
  not_if 'test -f /etc/apt/sources.list.d/kubernetes.list'
end
#
# => Check if an upgrade of Kubernetes should be done or not.
ruby_block 'prepare package options' do
  action :run
  block do
    if upgrading_kubernetes?
      Chef::Log.warn 'kubeadm: Kubernetes upgrade detected.'
      node.run_state['upgrading_kubernetes'] = true
    end
  end
end
#
# => Installs or upgrades the kubeadm, kubelet, and kubectl packages with
#    the same version as the Kubernetes version requested from the attributes.
package 'kubelet' do
  action %i[install lock]
  version(lazy { build_kubeadm_version })
  not_if { node.run_state['upgrading_kubernetes'] }
end
#
package 'kubectl' do
  action %i[install lock]
  version(lazy { build_kubeadm_version })
  not_if { node.run_state['upgrading_kubernetes'] }
end
#
# In the case of upgrading Kubernetes, only kubeadm is upgraded,
# then the cluster, and finally kubelete and kubectl (See `recipes/upgrades/`)
package 'kubeadm' do
  action %i[install lock]
  version(lazy { build_kubeadm_version })
  not_if { node.run_state['upgrading_kubernetes'] }
end
#
package 'kubeadm' do
  action %i[unlock install lock]
  version(lazy { build_kubeadm_version })
  only_if { node.run_state['upgrading_kubernetes'] }
end
#
# => Starts the kubelet service
service 'start kubelet' do
  service_name 'kubelet'
  action %i[enable start]
end
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
