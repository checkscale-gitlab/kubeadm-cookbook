# frozen_string_literal: true

# `fetch_password`
# `fetch_vrrp_id`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Keepalived)
# `kubernetes_master_reachable?`
Chef::Recipe.send(:include, KubeadmCookbook::Helper)

# The psmisc package provides the `/usr/bin/killall` command
package 'psmisc'

keepalived_install 'keepalived'

ruby_block 'node information grabbing' do
  action :run
  block do
    master_ip_address = detect_node_ip_address(as: :master) ||
                        ip_address_not_found!

    Chef::Log.warn 'kubeadm: Trying to ping the IP address ' \
                   "#{master_ip_address} ..."

    keepalived_master = shell_out(
      "ping -c 1 -W 1 #{master_ip_address}"
    ).exitstatus.positive?
    Chef::Log.warn 'kubeadm: This node is ' \
                   "seen#{' not' unless keepalived_master} as the keepalived " \
                   'master node. (keepalived_master: ' \
                   "#{keepalived_master.inspect})"
    node.run_state['keepalived_master'] = keepalived_master

    keepalived_password = fetch_password
    if keepalived_password.nil? || keepalived_password.empty?
      Chef::Log.fatal 'kubeadm: keepalived password is nil and will fail!'
      raise
    end

    node.run_state['keepalived_password'] = keepalived_password
    node.normal['kubeadm']['keepalived']['password'] = keepalived_password

    node.run_state['keepalived_router_id'] = fetch_vrrp_id
  end
end

keepalived_notify_script_path = nil

if node['kubeadm']['keepalived']['slack_webhook_url']
  keepalived_notify_script_path = '/usr/local/bin/keepalived_notify'
end

user 'keepalived_script'

# Slack notification script
template keepalived_notify_script_path do
  source 'keepalived_notify.erb'
  group 'keepalived_script'
  owner 'keepalived_script'
  mode '0755'
  variables webhook_url: node['kubeadm']['keepalived']['slack_webhook_url']
  not_if { keepalived_notify_script_path.nil? }
end

# global_defs
keepalived_global_defs 'global_defs' do
  enable_script_security true
  router_id node['hostname']
end

# vrrp_script
keepalived_vrrp_script 'haproxy-check' do
  interval 2
  script '"/usr/bin/killall -0 haproxy"'
  user 'root'
  weight 50
end

# vrrp_instance
keepalived_vrrp_instance 'kubernetes-vip' do
  authentication auth_type: 'PASS',
                 auth_pass: lazy { node.run_state['keepalived_password'] }
  interface node['kubeadm']['interface']
  master(lazy { node.run_state['keepalived_master'] })
  notify keepalived_notify_script_path
  priority(lazy { node.run_state['keepalived_master'] ? 110 : 100 })
  track_script %w[haproxy-check]
  unicast_src_ip node['kubeadm']['keepalived']['unicast_src_ip']
  unicast_peer node['kubeadm']['keepalived']['unicast_peer']
  virtual_ipaddress Array(node['kubeadm']['master_ip_address'])
  virtual_router_id(lazy { node.run_state['keepalived_router_id'] })

  notifies :restart, 'service[keepalived]', :immediately
end

#
# Systemd service unit linking.
#
systemd_partof = node['kubeadm']['keepalived']['systemd_partof']
Chef::Log.info "systemd_partof: #{systemd_partof.inspect}"
Chef::Log.info "/etc/systemd/system/keepalived.service.d/override.conf present?: #{File.exist?('/etc/systemd/system/keepalived.service.d/override.conf').inspect}"
if File.exist?('/etc/systemd/system/keepalived.service.d/override.conf')
  Chef::Log.info "/etc/systemd/system/keepalived.service.d/override.conf: #{File.read('/etc/systemd/system/keepalived.service.d/override.conf')}"
end

# systemd drop-in folder for keepalived
directory '/etc/systemd/system/keepalived.service.d/' do
  group 'root'
  mode  '0755'
  owner 'root'

  not_if { systemd_partof.nil? || systemd_partof.empty? }
  not_if "[ -d '/etc/systemd/system/keepalived.service.d/' ]"
end

# systemd drop-in file for keepalived which links the keepalived service to the
# given one.
# When the given service starts or stops, keepalived do the same.
template '/etc/systemd/system/keepalived.service.d/override.conf' do
  source 'keepalived.override.conf.erb'
  group 'root'
  owner 'root'
  mode '0777'
  variables service_name: node['kubeadm']['keepalived']['systemd_partof']

  # Runs the `systemctl daemon-reload` command when updating the file.
  notifies :run, 'ruby_block[reload systemd daemon]', :immediately

  not_if { systemd_partof.nil? || systemd_partof.empty? }
  not_if "grep \"PartOf=#{systemd_partof}\" " \
               '/etc/systemd/system/keepalived.service.d/override.conf'
end

ruby_block 'reload systemd daemon' do
  action :nothing # Should be triggered
  block do
    shell = shell_out('systemctl daemon-reload')
    Chef::Log.info "kubeadm: shell exitstatus: #{shell.exitstatus}"
    Chef::Log.info "kubeadm: shell stdout: #{shell.stdout}"
    Chef::Log.info "kubeadm: shell stderr: #{shell.stderr}"
  end
end

service 'keepalived' do
  action :enable
  subscribes :restart, 'template[/etc/systemd/system/keepalived.service.d/override.conf]', :immediately
end
