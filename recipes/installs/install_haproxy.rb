# frozen_string_literal: true

#
# Unfortunately the HAproxy cookbook cannot be used due to the fact that it
# uses the accumulator pattern, which makes the cookbook writing the HAproxy
# configuration file to the disk only at the end of the converg which makes the
# `kubeadm init` command to fail.
#

# `build_haproxy_server_list`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Haproxy)
# `detect_node_ip_address`
Chef::Resource::Template.send(:include, KubeadmCookbook::Helper)
# `sysctl_net_ipv4_ip_nonlocal_bind_should_be_update_for?`
Chef::Resource::Sysctl.send(:include, KubeadmCookbook::Haproxy)

# Install the haproxy package
package 'haproxy'

# This cookbook relies on the node's network interfaces, therefore it is very
# important to get the right information in order to prevent stupid failure due
# to no fresh data (other cookbook could mess up the Ohai data).
# This ohai block will reloads the network part of the extracted data when
# notifing it.
#
# (This means that this block is not executed immediately but when executing the
# following: `notifies :reload, 'ohai[reload_network]', :immediately`)
ohai 'reload_network' do
  action :reload
  plugin 'network'
end

log 'reload the Ohai network plugin' do
  level :warn
  message 'Reloading Ohai network plugin ...'
  notifies :reload, 'ohai[reload_network]', :immediately
end

sysctl 'net.ipv4.ip_nonlocal_bind' do
  value 1
  only_if { sysctl_net_ipv4_ip_nonlocal_bind_should_be_update_for?(node) }
end

ruby_block 'build HAproxy server list' do
  action :run
  block do
    node.run_state['haproxy_server_list'] = build_haproxy_server_list(
      as: :master
    )
  end
end

template '/etc/haproxy/haproxy.cfg' do
  source 'haproxy.cfg.erb'
  variables backend_server_list: lazy { node.run_state['haproxy_server_list'] },
            master_ip_address: detect_node_ip_address(as: :master),
            node_ip: detect_node_ip_address
end

service 'haproxy' do
  action :enable
  subscribes :restart, 'template[/etc/haproxy/haproxy.cfg]', :immediately
end
