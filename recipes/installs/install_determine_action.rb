# frozen_string_literal: true

# `ip_address_not_found!`
Chef::Recipe.send(:include, KubeadmCookbook::SanityCheck)

# Determines if the node should initialize, or join an existing cluster,
# this recipe checks if a Kubernetes is responding or not.
ruby_block 'determine the kubeadm action' do
  action :run
  block do
    master_node_count = master_nodes.count

    # This check prevent this cookbook believing there's already master nodes
    # when using a load balancer, because in this case, the TCP check would
    # be truthy while no master node would be up.
    if master_node_count.zero?
      node.run_state['first_control_plane'] = true
      Chef::Log.info 'This node is seen as the first control plane since ' \
                     'there are no saved master node in this Chef repo and ' \
                     'environment or policy group.'

      next
    end

    master_ip_address = search_master_ip_address || ip_address_not_found!
    master_port = detect_master_port(as: node.run_state['role'].to_sym)

    Chef::Log.warn "kubeadm: node['ipaddress']: #{node['ipaddress'].inspect}"
    Chef::Log.warn "kubeadm: master_ip_address: #{master_ip_address.inspect}"
    Chef::Log.warn "kubeadm: master_port: #{master_port.inspect}"

    master_reachable = kubernetes_master_reachable?(master_ip_address, master_port)
    Chef::Log.warn "kubeadm: master_reachable: #{master_reachable.inspect}"

    master_is_the_current_node = master_ip_address == node['ipaddress']
    Chef::Log.warn "kubeadm: master_is_the_current_node: #{master_is_the_current_node.inspect}"

    master_node = find_master_by_ip_address(master_ip_address)
    if master_node
      valid_master = valid_master_node?(master_node)
      Chef::Log.warn "kubeadm: valid_master: #{valid_master.inspect}"

      node_configuration_missing_keys = master_configuration_missing_keys(
        master_node
      )
      Chef::Log.warn "kubeadm: node_configuration_missing_keys: #{node_configuration_missing_keys.inspect}"
    end

    kubelet_conf_exist = ::File.exist?('/etc/kubernetes/kubelet.conf')
    Chef::Log.warn "kubeadm: kubelet_conf_exist: #{kubelet_conf_exist.inspect}"

    only_one_master = master_node_count == 1
    Chef::Log.warn "kubeadm: only_one_master: #{only_one_master.inspect}"

    if master_node && master_is_the_current_node && valid_master == false
      if only_one_master
        Chef::Log.warn 'This Kubernetes control plane is invalid (keys ' \
                       "#{node_configuration_missing_keys.join(', ')} are " \
                       'missing), so this cookbook will re-deploy Kubernetes ' \
                       'in order to fix its configuration.'
        node.run_state['first_control_plane'] = true
      else
        Chef::Log.warn 'This Kubernetes control plane is invalid (keys ' \
                       "#{node_configuration_missing_keys.join(', ')} are " \
                       'missing) and other existing control planes have been ' \
                       'found, so this cookbook will re-deploy Kubernetes ' \
                       'in order to fix its configuration.'
        node.run_state['first_control_plane'] = false
        node.run_state['control_plane_redeploy'] = true
      end
    end

    Chef::Log.warn "kubeadm: node.run_state['first_control_plane']: #{node.run_state['first_control_plane'].inspect}"
    Chef::Log.warn "kubeadm: node.run_state['control_plane_redeploy']: #{node.run_state['control_plane_redeploy'].inspect}"

    next if kubelet_conf_exist

    if master_reachable
      node.run_state['first_control_plane'] = false
      Chef::Log.info 'This node is seen as a second control plane.'
    else
      node.run_state['first_control_plane'] = true
      Chef::Log.info 'This node is seen as the first control plane.'
    end
  end
end
