# frozen_string_literal: true

if node['kubeadm']['multi_master'] == 'centralized' &&
   (node['kubeadm']['master_ip_address'].nil? ||
    node['kubeadm']['master_ip_address'].empty?)
  Chef::Log.fatal 'kubeadm: You have enabled the "centralized" multi_master ' \
                  "mode, but you haven't defined a Virtual IP address via " \
                  "the default['kubeadm']['master_ip_address'] attribute."
  Chef::Log.fatal 'kubeadm: Please update your configuration by setting the ' \
                  'desired VIP for your Kubernetes cluster, or by disabling ' \
                  'the multi_master mode by setting to "off" the ' \
                  "default['kubeadm']['multi_master'] attribute."

  raise
else
  # Keepalived configures a Virtual IP Address so that all the master nodes are
  # sharing a common IP address.
  include_recipe 'kubeadm::install_keepalived'

  # Determines kubeadm action (init or join).
  #
  # This is done after the Keepalived installation in order to allow new control
  # planes to be aware of existing other control planes.
  # For the first control plane, as there's no running kubeapi yet, it will not
  # be detected and the cookbook will know that it has to initialize the
  # cluster.
  include_recipe 'kubeadm::install_determine_action'

  # HAproxy is a load-balancer that will spread the requests to the Kubernetes
  # control planes.
  include_recipe 'kubeadm::install_haproxy'
end
