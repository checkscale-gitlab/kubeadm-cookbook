# frozen_string_literal: true

# `detect_master_port`
# `detect_node_ip_address`
# `fetch_discovery_token_ca_cert_hash_from_master`
# `fetch_token_from_master`
# `search_master_ip_address`
Chef::Recipe.send(:include, KubeadmCookbook::Helper)
# `ensure_node_network_configuration_will_match_attributes!`
Chef::Recipe.send(:include, KubeadmCookbook::SanityCheck)
# `discovery_token_ca_cert_hash_not_found!`
# `ip_address_not_found!`
# `kubernetes_token_not_found!`
# `master_ip_address_not_found!`
# `build_kubeadm_flags_from`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::SanityCheck)
# `first_valid_master_node`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Finders)
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::KubeadmFlags)
# `stream_command`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::StreamCommand)

ensure_node_network_configuration_will_match_attributes!

ruby_block 'node information grabbing' do
  action :run
  block do
    master = first_valid_master_node
    decentralized = master['kubeadm']['multi_master'] == 'decentralized'

    node.run_state['master_ip_address'] = if decentralized
                                            '127.0.0.1'
                                          else
                                            search_master_ip_address(master) ||
                                              master_ip_address_not_found!
                                          end

    node.run_state['master_port'] = if decentralized
                                      6443
                                    else
                                      detect_master_port
                                    end

    node.run_state['kubeadm_token'] = fetch_token_from_master ||
                                      kubernetes_token_not_found!
    node.run_state['discovery_token_ca_cert_hash'] = begin
      fetch_discovery_token_ca_cert_hash_from_master ||
        discovery_token_ca_cert_hash_not_found!
    end

    Chef::Log.info 'kubeadm: This node will use the IP Address ' \
                   "#{node.run_state['master_ip_address'].inspect} with " \
                   "kubeadm on port #{node.run_state['master_port'].inspect} " \
                   'when joining the cluster using the token ' \
                   "#{node.run_state['kubeadm_token'].inspect} and CA cert " \
                   "#{node.run_state['discovery_token_ca_cert_hash'].inspect}."
  end
  not_if "grep 'https://#{node.run_state['master_ip_address']}' " \
               '/etc/kubernetes/kubelet.conf'
end

# Joins the Kubernetes master node
ruby_block 'kubeadm join' do
  action :run
  block do
    hash = "sha256:#{node.run_state['discovery_token_ca_cert_hash']}"

    ip_address = node.run_state['master_ip_address']
    port = node.run_state['master_port']

    command = <<-CMD
      kubeadm join \
        #{build_kubeadm_flags_from(
          "#{ip_address}:#{port}": '',
          '--token': node.run_state['kubeadm_token'],
          '--discovery-token-ca-cert-hash': hash,
          '--v': 5
        )}
    CMD

    Chef::Log.warn "\nkubeadm: Joining Kubernetes master with command " \
                   "`#{command}` ..."

    error_detected = false

    stream_command(command) do |output|
      cleaned_output = output.gsub(/\r\n/, '').gsub(/\t/, '  ')
      Chef::Log.warn cleaned_output

      if cleaned_output =~ /error|failed/
        Chef::Log.warn "\nkubeadm: An error has been detected from the " \
                       'kubadm command output.'
        error_detected = true
      end
    end

    if error_detected
      Chef::Log.error 'kubeadm: An error occured while joining the master ' \
                      'node. Please check that error and retry.'
      Chef::Log.info 'kubeadm: In the case you have a an unauthorized error, ' \
                      'converge a master node so that the cookbook will ' \
                      'retrieve a fresh token and cert hash.'

      raise
    end
  end
  not_if "grep 'https://#{node.run_state['master_ip_address']}' " \
               '/etc/kubernetes/kubelet.conf'
end

ruby_block 'Fetches the current node IP address' do
  action :run
  block do
    node.run_state['node_ip'] = detect_node_ip_address(as: :worker) ||
                                ip_address_not_found!
  end
end

# template new kubelet config file
template '/etc/default/kubelet' do
  source 'kubelet.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables node_ip: lazy { node.run_state['node_ip'] }
  not_if "grep '--node-ip=#{node.run_state['node_ip']}' " \
               '/etc/default/kubelet'

  notifies :run, 'execute[systemd daemon reload]', :immediately
end

execute 'systemd daemon reload' do
  action :nothing
  command 'systemctl daemon-reload'

  notifies :restart, 'service[restart kubelet]', :immediately
end

# Restart kubelet
service 'restart kubelet' do
  action [:nothing]
  service_name 'kubelet'
  supports status: true
end
