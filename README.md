# kubeadm Cookbook

Chef cookbook to create a Kubernetes cluster using the kubeadm tool.

This cookbook has been made based in the official documents: https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/

This cookbook supports:

 - Bootstrapping a single master cluster
 - Bootstrapping a "centralized" multi-master cluster (master nodes are behind a software load balancer) for private network
 - Bootstrapping a "decentralized" multi-master cluster (workers has a local load balancer knowing the masters' IP addresses) for public network
 - Bootstrapping a "balanced" multi-master cluster (all nodes are refering to an external Load Balancer)
 - Upgrading Kubernetes in single and multi-master cluster mode
 - Enabling the Pod Security Policy feature

## Requirements
### Cookbooks

 - A container runtime (Docker, CRI-O, Containerd, ...)
 - [keepalived](https://supermarket.chef.io/cookbooks/keepalived)

*Note*: This cookbook is only tested with [the docker-ce cookbook](gitlab.com/pharmony/docker-ce-cookbook).

### Platforms

The following platforms are supported and tested with Test Kitchen:

- Debian 9/10
- Ubuntu 18.04

### Chef

- Chef 12.1+

## Attributes

Please see the `attributes/default.rb` file, there are plenty of comments explaining them

## Tested Kubernetes versions

This cookbook follow the versions from the [Supported Versions of the Kubernetes Documentation](https://kubernetes.io/docs/home/supported-doc-versions/).

See also the [Kubernetes Release Versioning](https://github.com/kubernetes/community/blob/master/contributors/design-proposals/release/versioning.md) documentation.

* SM: Single Master
* MM: Multi Master
* UP: Kubernetes Upgrade from the previous version

* ✔️ Passing tests suite
* 🚫️ Not supported
* ⚠️ Almost passing but with warning(s)
* ❌️ Doesn't work at the moment
* ❔️ Unknown, to be checked

### Debian

| Kubernetes | Platform | flannel (0.12.0)      | Calico (3.18)        |
| ---------- | -------- | --------------------- | -------------------- |
| v1.22      | Buster   | SM: ❔️ MM: ❔️ UP: ❔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.22      | Stretch  | SM: ❔️ MM: ❔️ UP: ❔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.21      | Buster   | SM: ❔️ MM: ❔️ UP: ❔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.21      | Stretch  | SM: ❔️ MM: ❔️ UP: ❔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.20      | Buster   | SM: ✔️ MM: ✔️ UP: ❌️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.20      | Stretch  | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.19      | Buster   | SM: ✔️ MM: ✔️ UP: ❌️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.19      | Stretch  | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.18      | Buster   | SM: ✔️ MM: ✔️ UP: ❌️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.18      | Stretch  | SM: ✔️ MM: ✔️ UP: ❌️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.17      | Buster   | SM: ✔️ MM: ✔️ UP: ❌️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.17      | Stretch  | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.16      | Buster   | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.16      | Stretch  | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.15      | Buster   | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.15      | Stretch  | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ❔️ MM: ❔️ UP: ❔️ |

#### Issues

* v1.20 Buster : Remain stuck on `[addons] Applied essential addon: CoreDNS`
* v1.19 Buster : Remain stuck on `[addons] Applied essential addon: CoreDNS`
* v1.18 Buster : "Killed" during the master upgrade
* v1.18 Stretch : "net/http: request canceled" during the master upgrade
* v1.17 Buster : Remain stuck on `[addons] Applied essential addon: CoreDNS`

### Ubuntu

| Kubernetes | Platform | flannel (0.12.0)      | Calico (3.18)        |
| ---------- | -------- | --------------------- | -------------------- |
| v1.22      | 18.04    | SM: ❔️ MM: ❔️ UP: ❔️ | SM: ✔️ MM: ✔️ UP: ✔️ |
| v1.21      | 18.04    | SM: ❔️ MM: ❔️ UP: ❔️ | SM: ✔️ MM: ✔️ UP: ✔️ |
| v1.20      | 18.04    | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ✔️ MM: ✔️ UP: ✔️ |
| v1.19      | 18.04    | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.18      | 18.04    | SM: ✔️ MM: ✔️ UP: ❌️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.17      | 18.04    | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.16      | 18.04    | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ❔️ MM: ❔️ UP: ❔️ |
| v1.15      | 18.04    | SM: ✔️ MM: ✔️ UP: ✔️ | SM: ❔️ MM: ❔️ UP: ❔️ |

#### Issues

* v1.18 18.04 : "Killed" during the master upgrade

### Adding a Kubernetes version

1. Duplicate an existing version (master, multi master nodes and worker node blocks) from the `suites` section
2. Update the kubeadm version to the latest version of the serie you'd like to test
3. Run the tests (See the `TESTING.md` file)
4. Update the `README.md` file with the tested version
5. When tests are passing, open a merge request

## Usage

### Bootstrapping a new cluster

TODO : Write more documentation on how to configure a Kubernetes custer with config examples.

In order to use this cookbook:

1. Add the cookbook to your chef repository.
2. Add the `kubeadm::master` recipe to the node that shall run the master node.
3. Add the `kubeadm::node` recipe to the nodes that shall be Kubernetes worker nodes
4. Converge ! :)

### Upgrading an existing cluster

1. Update the kubeadm version attribute
2. Converge one control plane node first (master)
3. Converge the other control plane nodes (in multi master mode)
4. If all went well, converge the worker nodes one after the other

Wait for the converged node to be in 'Ready' state before to converge another one in order to prevent any replication issues.

## Kubernetes Container Network Interface (CNI) Drivers

This cookbook has been made so that it can support many [CNI drivers](https://kubernetes.io/docs/concepts/cluster-administration/addons/#networking-and-network-policy).

Supported drivers:

 - flannel (https://github.com/coreos/flannel) *default*
 - calico (https://www.projectcalico.org/)

### How to add new drivers?

Drivers are configured by giving its name via `default['kubeadm']['cni_driver']['name']` and its version via `default['kubeadm']['cni_driver']['version']`.

The CNI driver recipe is ran from the Kubernetes master install recipe which is located at the bottom in the `recipes/installs/install_master.rb` file. To do so, it takes the `default['kubeadm']['cni_driver']['name']`, applies a `downcase` on it, and load the file with that name stored in the `recipes/drivers/` folder.

For example, let's say you'd like to implement the Romana driver, you have to add the `recipes/drivers/romana.rb` file, and then use the `romana` value for the `default['kubeadm']['cni_driver']['name']` attribute.

The recipe must finish when the network container is up and in the `Running` state (Have a look at the fannel implementation).
