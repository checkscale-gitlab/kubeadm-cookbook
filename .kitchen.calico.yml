---
driver:
  name: vagrant

provisioner:
  # You may wish to disable always updating cookbooks in CI or other testing environments.
  # For example:
  #   always_update_cookbooks: <%= !ENV['CI'] %>
  always_update_cookbooks: true
  chef_license: accept
  log_level: info
  name: chef_zero
  product_name: chef
  product_version: 17.9.26

verifier:
  name: inspec

platforms:
  - name: debian/stretch64
  - name: debian/buster64
  - name: ubuntu/bionic64

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Default master and worker attributes
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.master: &master
  driver:
    customize:
      memory: 2048
    network:
      - ["private_network", { ip: "172.28.128.200" }]
    vm_hostname: master-calico.vagrant.test
  run_list:
    - recipe[apt]
    - recipe[docker-ce]
    - recipe[kubeadm::master]
  verifier:
    inspec_tests:
      - test/integration/master/calico_test.rb
.multi-master1: &multi-master1
  driver:
    customize:
      memory: 2048
    network:
      - ["private_network", { ip: "172.28.128.200" }]
    vm_hostname: master-1-calico.vagrant.test
  provisioner:
    nodes_path: test/fixtures/multi-master/phase-1
  run_list:
    - recipe[apt]
    - recipe[docker-ce]
    - recipe[kubeadm::master]
  verifier:
    inspec_tests:
      - test/integration/multi-master/decentralized/master-1/calico_test.rb
.multi-master2: &multi-master2
  driver:
    customize:
      memory: 2048
    network:
      - ["private_network", { ip: "172.28.128.201" }]
    vm_hostname: master-2-calico.vagrant.test
  provisioner:
    nodes_path: test/fixtures/multi-master/phase-2/calico
  run_list:
    - recipe[apt]
    - recipe[docker-ce]
    - recipe[kubeadm::master]
  verifier:
    inspec_tests:
      - test/integration/multi-master/decentralized/master-2/calico_test.rb
.worker: &worker
  driver:
    customize:
      memory: 2048
    network:
      - ["private_network", { ip: "172.28.128.220" }]
    vm_hostname: worker-calico.vagrant.test
  provisioner:
    nodes_path: test/fixtures/nodes/calico
  run_list:
    - recipe[apt]
    - recipe[docker-ce]
    - recipe[kubeadm::worker]
  verifier:
    inspec_tests:
      - test/integration/worker_test.rb
.multi-master-worker: &multi-master-worker
  driver:
    customize:
      memory: 2048
    network:
      - ["private_network", { ip: "172.28.128.220" }]
    vm_hostname: worker-calico.vagrant.test
  provisioner:
    nodes_path: test/fixtures/multi-master/phase-2/calico
  run_list:
    - recipe[apt]
    - recipe[docker-ce]
    - recipe[kubeadm::worker]
  verifier:
    inspec_tests:
      - test/integration/worker_test.rb
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

suites:
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Kubernetes install
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  #
  # Latest Kubernetes
  - name: k8s-latest-master-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: master
        interface: eth1
        ip_address_version: ipv4
        version: latest
  - name: k8s-latest-mutli-master-1-calico
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: master
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: latest
  - name: k8s-latest-mutli-master-2-calico
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: master
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: latest
  - name: k8s-latest-mutli-master-worker-calico
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: latest
  - name: k8s-latest-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        version: latest
  #
  # Kubernetes 1.22.x
  - name: k8s-1-22-master-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.22.0
  - name: k8s-1-22-mutli-master-1-calico
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.22.0
  - name: k8s-1-22-mutli-master-2-calico
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.22.0
  - name: k8s-1-22-mutli-master-worker-calico
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.22.0
  - name: k8s-1-22-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        version: 1.22.0
  #
  # Kubernetes 1.21.x
  - name: k8s-1-21-master-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.21.0
  - name: k8s-1-21-mutli-master-1-calico
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.21.0
  - name: k8s-1-21-mutli-master-2-calico
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.21.0
  - name: k8s-1-21-mutli-master-worker-calico
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.21.0
  - name: k8s-1-21-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        version: 1.21.0
  #
  # Kubernetes 1.20.x
  - name: k8s-1-20-master-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.20.0
  - name: k8s-1-20-mutli-master-1-calico
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.20.0
  - name: k8s-1-20-mutli-master-2-calico
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.20.0
  - name: k8s-1-20-mutli-master-worker-calico
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.20.0
  - name: k8s-1-20-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        version: 1.20.0
  #
  # Kubernetes 1.19.x
  - name: k8s-1-19-master-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.19.0
  - name: k8s-1-19-mutli-master-1-calico
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.19.0
  - name: k8s-1-19-mutli-master-2-calico
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.19.0
  - name: k8s-1-19-mutli-master-worker-calico
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.19.0
  - name: k8s-1-19-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        version: 1.19.0
  #
  # Kubernetes 1.18.x
  - name: k8s-1-18-master-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.18.0
  - name: k8s-1-18-mutli-master-1-calico
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.18.0
  - name: k8s-1-18-mutli-master-2-calico
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.18.0
  - name: k8s-1-18-mutli-master-worker-calico
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.18.0
  - name: k8s-1-18-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        version: 1.18.0
  #
  # Kubernetes 1.17.x
  - name: k8s-1-17-master-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.17.4
  - name: k8s-1-17-mutli-master-1-calico
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.17.4
  - name: k8s-1-17-mutli-master-2-calico
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.17.4
  - name: k8s-1-17-mutli-master-worker-calico
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.17.4
  - name: k8s-1-17-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        version: 1.17.4
  #
  # Kubernetes 1.16.x
  - name: k8s-1-16-master-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.16.15
  - name: k8s-1-16-mutli-master-1-calico
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.16.15
  - name: k8s-1-16-mutli-master-2-calico
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.16.15
  - name: k8s-1-16-mutli-master-worker-calico
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.16.15
  - name: k8s-1-16-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        version: 1.16.15
  #
  # Kubernetes 1.15.x
  - name: k8s-1-15-master-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.15.11
  - name: k8s-1-15-mutli-master-1-calico
    <<: *multi-master1
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.15.11
  - name: k8s-1-15-mutli-master-2-calico
    <<: *multi-master2
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.15.11
  - name: k8s-1-15-mutli-master-worker-calico
    <<: *multi-master-worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.15.11
  - name: k8s-1-15-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        version: 1.15.11
  #
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Kubernetes install
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Kubernetes upgrade
  #
  # This part describes the attributes of the converge attributes which will
  # engage the Kubernetes upgrade.
  #
  # The `bin/test-kubernetes-upgrade` script first uses the
  # `.kitchen.upgrades.yml` file in order to create a master/worker cluster and
  # then uses this `.kitchen.yml` file in order to converge the nodes with the
  # updated attributes triggering the Kubernetes upgrade.
  # See the `.kitchen.upgrades.yml` file and the `bin/test-kubernetes-upgrade`
  # script.
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  #
  # Upgrades Kubernetes from 1.21 to 1.22
  - name: k8s-upgrade-1-22-mutli-master-1-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.22.0
    verifier:
      inspec_tests:
        - test/integration/multi-master/decentralized/master-1/calico_test.rb
        - test/integration/upgrade/1-22_test.rb
  - name: k8s-upgrade-1-22-mutli-master-2-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.22.0
    verifier:
      inspec_tests:
        - test/integration/multi-master/decentralized/master-2/calico_test.rb
        - test/integration/upgrade/1-22_test.rb
  - name: k8s-upgrade-1-22-mutli-master-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.22.0
    verifier:
      inspec_tests:
        - test/integration/worker_test.rb
        - test/integration/upgrade/1-22_test.rb
  #
  # Upgrades Kubernetes from 1.20 to 1.21
  - name: k8s-upgrade-1-21-mutli-master-1-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.21.0
    verifier:
      inspec_tests:
        - test/integration/multi-master/decentralized/master-1/calico_test.rb
        - test/integration/upgrade/1-21_test.rb
  - name: k8s-upgrade-1-21-mutli-master-2-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.21.0
    verifier:
      inspec_tests:
        - test/integration/multi-master/decentralized/master-2/calico_test.rb
        - test/integration/upgrade/1-21_test.rb
  - name: k8s-upgrade-1-21-mutli-master-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.21.0
    verifier:
      inspec_tests:
        - test/integration/worker_test.rb
        - test/integration/upgrade/1-21_test.rb
  #
  # Upgrades Kubernetes from 1.19 to 1.20
  - name: k8s-upgrade-1-20-mutli-master-1-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.20.1
    verifier:
      inspec_tests:
        - test/integration/multi-master/decentralized/master-1/calico_test.rb
        - test/integration/upgrade/1-20_test.rb
  - name: k8s-upgrade-1-20-mutli-master-2-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.20.1
    verifier:
      inspec_tests:
        - test/integration/multi-master/decentralized/master-2/calico_test.rb
        - test/integration/upgrade/1-20_test.rb
  - name: k8s-upgrade-1-20-mutli-master-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.20.1
    verifier:
      inspec_tests:
        - test/integration/worker_test.rb
        - test/integration/upgrade/1-20_test.rb
  #
  # Upgrades Kubernetes from 1.18 to 1.19
  - name: k8s-upgrade-1-19-mutli-master-1-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.19.6
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-19_test.rb
  - name: k8s-upgrade-1-19-mutli-master-2-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.19.6
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-19_test.rb
  - name: k8s-upgrade-1-19-mutli-master-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.19.6
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-19_calico/worker_test.rb
  #
  # Upgrades Kubernetes from 1.17 to 1.18
  - name: k8s-upgrade-1-18-mutli-master-1-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.18.14
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-18_test.rb
  - name: k8s-upgrade-1-18-mutli-master-2-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.18.14
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-18_test.rb
  - name: k8s-upgrade-1-18-mutli-master-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.18.14
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-18_calico/worker_test.rb
  #
  # Upgrades Kubernetes from 1.16 to 1.17
  - name: k8s-upgrade-1-17-mutli-master-1-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.17.16
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-17_test.rb
  - name: k8s-upgrade-1-17-mutli-master-2-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.17.16
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-17_test.rb
  - name: k8s-upgrade-1-17-mutli-master-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.17.16
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-17_calico/worker_test.rb
  #
  # Upgrades Kubernetes from 1.15 to 1.16
  - name: k8s-upgrade-1-16-mutli-master-1-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.16.15
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-16_test.rb
  - name: k8s-upgrade-1-16-mutli-master-2-calico
    <<: *master
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        cni_driver:
          name: calico
          version: 3.18
        interface: eth1
        ip_address_version: ipv4
        version: 1.16.15
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-16_test.rb
  - name: k8s-upgrade-1-16-mutli-master-worker-calico
    <<: *worker
    attributes:
      docker_ce:
        daemon:
          cgroup_driver: systemd
        version: 18.09
      kubeadm:
        interface: eth1
        ip_address_version: ipv4
        multi_master: decentralized
        version: 1.16.15
    verifier:
      inspec_tests:
        - test/integration/upgrade/1-16_calico/worker_test.rb
  #
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Kubernetes upgrade
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
