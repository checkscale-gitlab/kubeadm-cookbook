# frozen_string_literal: true

# Inspec test for recipe .::node

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe command('swapon -s | wc -l | grep 0') do
  its('exit_status') { should eq 0 }
end

describe service('firewalld') do
  it { should_not be_running }
end

describe package('apt-transport-https') do
  it { should be_installed }
end

# describe apt('https://apt.kubernetes.io/') do
#   it { should exist }
#   it { should be_enabled }
# end

describe package('kubeadm') do
  it { should be_installed }
end

describe service('kubelet') do
  it { should be_running }
end

describe file('/etc/kubernetes/kubelet.conf') do
  it { should exist }
end

describe file('/etc/default/kubelet') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0755' }
  its('content') do\
    should match(%r{KUBELET_EXTRA_ARGS=.*--node-ip=172.28.128.220})
  end
end

describe processes('dockerd') do
  it { should exist }
end

describe processes('kubelet') do
  it { should exist }
end
