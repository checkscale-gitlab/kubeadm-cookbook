# frozen_string_literal: true

require 'pty'

module KubeadmCookbook
  class WaitCommandTimeoutError < StandardError; end

  #
  # Implements the `stream_command` which allows you to run a command and get
  # its output in real-time.
  #
  module StreamCommand
    def stream_command(command, options = {})
      PTY.spawn(command) { |stdout, _, _| loop { yield stdout.gets } }
    rescue Errno::EIO
      raise if options[:raise_on_error]
    end

    #
    # Runs the given command, streams the command's output, and watch the output
    # for any errors matching the error_regex.
    #
    def stream_command_and_watch_for(command, match_regex, ignore_errors = [])
      error_detected = false
      command_output_matched_regex = false
      ignore_errors = Array(ignore_errors) # Makes an array if it is not

      # * strip removes begining and trailing spaces
      # * gsub(/\s{2}/, ' ') replaces multiple spaces in a single one
      # * gsub(/\n$/, '') removes trailing newline if any (strip should remove
      #                   them too)
      formatted_command = command.strip.gsub(/\s{2}/, ' ').gsub(/\n$/, '')

      Chef::Log.info "kubeadm: Running command #{formatted_command.inspect} ..."

      stream_command(formatted_command) do |output|
        message = output.gsub(/\r\n/, '').gsub(/\t/, '  ')

        if error_detected == false && message =~ /error/
          if ignore_errors.empty?
            # When the "error" word has been found, and the ignore_errors param
            # is empty, then make the execution of the command as failed.
            error_detected = true
          else
            # When the "error" word has been found, and the ignore_errors
            # param has at least one value ...
            if ignore_errors.any? { |error| message =~ error }
              # And one of them matches the error message, then skip it
            else
              error_detected = true
            end
          end
        end

        command_output_matched_regex = true if message =~ match_regex

        Chef::Log.info "kubeadm: #{message}"
      end

      return true if error_detected == false || command_output_matched_regex

      Chef::Log.fatal 'kubeadm: An error has been detected from the output ' \
                      "of the #{formatted_command.inspect} command."

      raise
    end

    def wait_command_output(command, match_regex, options = {})
      options[:timeout] ||= 60

      previous_command_status = nil
      try_count = 0

      while previous_command_status !~ match_regex
        # Run the command until it ends
        stream_command command do |output|
          command_output = output.gsub(/\r\n/, '')

          # yield the command output only when it changes
          unless previous_command_status == command_output
            previous_command_status = command_output

            yield command_output if block_given?
          end
        end

        #
        # Timeout management
        #
        try_count += 1

        raise WaitCommandTimeoutError if try_count >= options[:timeout]

        sleep 1
      end
    end
  end
end
