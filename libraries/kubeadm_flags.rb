# frozen_string_literal: true

module KubeadmCookbook
  #
  # Kubeadm flags builder for the Kubeadm cookbook
  #
  module KubeadmFlags
    # formatted_cni_driver_name
    include KubeadmCookbook::Formatters

    #
    # Build the final `kubeadm init/join` command flags from the given arguments
    #
    # It avoids passing empty flags, reducing noise in the logs
    # It forces values when needed (like `--pod-network-cidr` when using flannel
    # network driver).
    #
    def build_kubeadm_flags_from(args)
      flags = []

      args.each_key { |key| flags << kubeadm_flag_for(key.to_s, args[key]) }

      flags = flags.compact.join(' ')

      Chef::Log.warn "\nkubeadm: Using flags #{flags} to run the `kubeadm` " \
                     'command ...'

      flags
    end

    private

    def kubeadm_flag_for(key, value)
      # This case statement treat only exceptions, all the rest is treated the
      # same from the `else` part which returns key=value if there is a value.
      case key
      # Network drivers could require to force the value of this flag like
      # flannel does.
      when '--pod-network-cidr'
        kubeadm_flag_or_nil_from(key,
                                 forced_or_initial_for_network_driver(value))
      else
        # Avoids empty flags
        kubeadm_flag_or_nil_from(key, value)
      end
    end

    # Forces the value when the network driver required a forced value
    # (like Flannel requires this flag to be ),
    # otherwise it returns the given value.
    def forced_or_initial_for_network_driver(value)
      case formatted_cni_driver_name
      when 'flannel' then '10.244.0.0/16'
      else
        value
      end
    end

    # { '--my-flag': nil }
    # => nil
    #
    # { '--my-flag': {} }
    # => nil
    #
    # { '--my-flag': '' }
    # => '--my-flag'
    #
    # { '--my-flag': 'my-val' }
    # => '--my-flag=my-val'
    #
    def kubeadm_flag_or_nil_from(key, value)
      return nil if value == {} || value.nil?

      value == '' ? key : "#{key}=#{value}"
    end
  end
end
