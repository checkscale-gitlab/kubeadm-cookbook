# frozen_string_literal: true

module KubeadmCookbook
  #
  # Kubeadm cookbook helper methods to query Chef
  #
  module Finders
    def find_master_by_ip_address(ip_address)
      masters = master_nodes

      if masters.empty?
        Chef::Log.warn 'kubeadm: Cannot retrieve a valid master: ' \
                       'master list is empty.'
        return
      end

      masters.detect { |master| detect_ip_address_from(master) == ip_address }
    end

    #
    # Returns the first master node which has all the required keys set
    #
    def first_valid_master_node
      masters = master_nodes

      if masters.empty?
        Chef::Log.warn 'kubeadm: Cannot retrieve a valid master: ' \
                       'master list is empty.'
        return
      end

      masters.detect { |master| valid_master_node?(master) }
    end

    def master_configuration_missing_keys(master)
      keys = []

      keys << 'kubeadm' unless master.key?('kubeadm')

      keys << 'token' if node_is_missing?(master, 'token')
      keys << 'certificate_key' if node_is_missing?(master, 'certificate_key')
      if node_is_missing?(master, 'discovery_token_ca_cert_hash')
        keys << 'discovery_token_ca_cert_hash'
      end
      keys << 'kube_config' if node_is_missing?(master, 'kube_config')

      keys
    end

    def master_nodes
      @master_nodes ||= begin
        query = 'run_list:*kubeadm??master*'

        query += " AND policy_group: #{node.policy_group}" if node.policy_group

        Chef::Log.warn 'kubeadm: Searching master nodes with query ' \
                       "#{query.inspect}"
        nodes = search(:node, query)

        Chef::Log.warn "kubeadm: #{nodes.size} master node(s) found using " \
                       "the query #{query.inspect}."

        Array(nodes)
      end
    end

    def node_is_missing?(node, kubeadm_key)
      return true unless node['kubeadm'].key?(kubeadm_key)

      node['kubeadm'][kubeadm_key] && node['kubeadm'][kubeadm_key].empty?
    end

    def valid_master_node?(master)
      return false unless master.key?('kubeadm')

      return false if node_is_missing?(master, 'token')
      return false if node_is_missing?(master, 'certificate_key')
      return false if node_is_missing?(master, 'discovery_token_ca_cert_hash')
      return false if node_is_missing?(master, 'kube_config')

      true
    end
  end
end
