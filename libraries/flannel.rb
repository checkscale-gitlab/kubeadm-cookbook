# frozen_string_literal: true

require 'yaml'

module KubeadmCookbook
  #
  # Flannel network driver helpers.
  # https://github.com/coreos/flannel
  #
  module Flannel
    class FlannelContainerNotFoundError < StandardError; end

    # Depending on the flannel version, the YAML file update is different.
    # Before version 0.10.0, the `kube-flannel` container command was written
    # that way:
    #  - name: kube-flannel
    #    command: [ "/opt/bin/flanneld", "--ip-masq", "--kube-subnet-mgr" ]
    #
    # Since version 0.10.0 it is written:
    #  - name: kube-flannel
    #    command:
    #    - /opt/bin/flanneld
    #    args:
    #    - --ip-masq
    #    - --kube-subnet-mgr
    #
    # This method updates the config file in order to add the --iface flag
    def update_flannel_config_file_at(path)
      # YAML.load_stream to read multiple documents in a single file.
      # See https://ruby-doc.org/stdlib-2.2.2/libdoc/psych/rdoc/Psych.html#method-c-load_stream
      configs = YAML.load_stream(File.read(path))

      update_flannel_configs_with(
        configs,
        "--iface=#{node['kubeadm']['interface']}",
        flannel_version_from_attributes
      )

      write_yaml_documents_to(path, configs)
    rescue FlannelContainerNotFoundError
      log_invalid_yaml_file_at(path)
      raise
    end

    def flannel_url_with_version
      url = 'https://raw.githubusercontent.com/coreos/flannel/' \
            "#{flannel_version}/Documentation/kube-flannel.yml"

      Chef::Log.warn "\nkubeadm: Downloading Flannel YAML file at #{url} ..."
      url
    end

    private

    def flannel_version
      version = flannel_version_from_attributes

      if version.nil? || version == 'master' || version == 'latest'
        'master'
      else
        "v#{version}"
      end
    end

    def flannel_version_from_attributes
      return nil unless node['kubeadm']['cni_driver']

      node['kubeadm']['cni_driver']['version']
    end

    def log_invalid_yaml_file_at(path)
      Chef::Log.fatal "kubeadm: The flannel YAML file at #{path} doesn't " \
                      'seem to have a kube-flannel container where to add ' \
                      'the --iface flag to the command attribute.'
      Chef::Log.fatal 'kubadm: Please review this file and if you think this ' \
                      'is a bug, please open an issue in the kubeadm ' \
                      'cookbook project repository'
    end

    def update_flannel_configs_with(documents, flag, version)
      documents.each do |document|
        next unless document['kind'] == 'DaemonSet'

        update_deamon_command_flags_with(document, flag, version)
      end
    end

    def update_container_with(kube_flannel, flag, version)
      # Latest version or a version higher or equal to 0.10.0
      if version.nil? || version == 'master' || version == 'latest' ||
         Gem::Version.new(version) >= Gem::Version.new('0.10.0')
        kube_flannel['args'] << flag
      else
        kube_flannel['command'] << flag
      end
    end

    def update_deamon_command_flags_with(config, flag, version)
      containers = config['spec']['template']['spec']['containers']

      kube_flannel = containers.detect do |container|
        container['name'] == 'kube-flannel'
      end

      raise FlannelContainerNotFoundError unless kube_flannel

      update_container_with(kube_flannel, flag, version)
    end

    def write_yaml_documents_to(path, documents)
      File.open(path, 'w') do |file|
        documents.each do |document|
          # YAML.dump
          file.write(document.to_yaml)
        end
      end
    end
  end
end
